# README #

This Project includes the all answers of the Homework 03 (Kube-Exercises Part II)

### Answer-Exercise-01 includes: ###

* answer-exercise-01-hw03.pdf (description and step by step to get the final result). Download de PDF
* deployment.yml
* service.yml
* ingress.yml
* tls.crt (cert.)
* tls.key (secret)

### Answer-Exercise-02 includes: ###

* answer-exercise-02-hw03.pdf (description and step by step to get the final result). Download de PDF
* mongodb-service.yml
* mongodb-statefulset.yml

### Answer-Exercise-03 includes: ###

* answer-exercise-03-hw03.pdf (description and step by step to get the final result). Download de PDF
* deploy.yml
* hpa.yml
* service.yml